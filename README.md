git-diff-all
============

## About

`git-diff-all`, as the name suggests, is a little script to check if all the repositories in the current directories are up to date with their remote copies. Literally, it just loops over the first level subdirectories of the current dir, and then runs `git diff` for the active branch. Errors and non-git subdirectories are ignored.

## Usage

Just run the provided script. Best used with the [`git-status-behind-all`](https://bitbucket.org/dsjkvf/git-status-behind-all) script, which checks if the repositories in the current directory aren't lagging behind their remote copies.
